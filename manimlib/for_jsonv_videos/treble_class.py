from manimlib.constants import *
from manimlib.for_jsonv_videos.treble_creature import TrebleCreature
from manimlib.mobject.types.vectorized_mobject import VGroup


class TrebleCreatureClass(VGroup):
    CONFIG = {
        "width": 3,
        "height": 2
    }

    def __init__(self, **kwargs):
        VGroup.__init__(self, **kwargs)
        for i in range(self.width):
            for j in range(self.height):
                treble = TrebleCreature().scale(0.3)
                treble.move_to(i * DOWN + j * RIGHT)
                self.add(treble)

