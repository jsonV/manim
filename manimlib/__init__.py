#!/usr/bin/env python
import manimlib.config
import manimlib.constants
import manimlib.extract_scene
import manimlib.stream_starter


def main():
    print("Getting CLI arguments...")
    args = manimlib.config.parse_cli()
    print("Retireved args: ", args)
    if not args.livestream:
        print("Configuring manim with args...")
        config = manimlib.config.get_configuration(args)
        print("Manim configuration: ",config)
        print()
        print("Initializing directories...")
        manimlib.constants.initialize_directories(config)
        print("Extracting scene...")
        manimlib.extract_scene.main(config)
    else:
        manimlib.stream_starter.start_livestream(
            to_twitch=args.to_twitch,
            twitch_key=args.twitch_key,
        )
